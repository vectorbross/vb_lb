<?php

namespace Drupal\vb_lb\Element;

use Drupal\layout_builder\Element\LayoutBuilder;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Alters the Layout Builder UI element.
 */
class VbLayoutBuilder extends LayoutBuilder {

  /**
   * Builds a link to add a new section at a given delta.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   *
   * @return array
   *   A render array for a link.
   */
  protected function buildAddSectionLink(SectionStorageInterface $section_storage, $delta) {
    $link = parent::buildAddSectionLink($section_storage, $delta);

    if(!\Drupal::currentUser()->hasPermission('add empty sections')) {
      $link['link']['#access'] = FALSE;
    }
    
    return $link;
  }

}
