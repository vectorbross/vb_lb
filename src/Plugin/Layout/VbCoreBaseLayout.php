<?php

namespace Drupal\vb_lb\Plugin\Layout;

use Drupal\bootstrap_layout_builder\Plugin\Layout\BootstrapLayout;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;

/**
 * Class VbCoreBaseLayout.
 *
 * @package Drupal\vb_lb\Plugin\Layout
 * 
 * @Layout(
 *   id = "bootstrap_layout_builder",
 *   deriver = "Drupal\vb_lb\Plugin\Deriver\VbCoreBaseLayoutDeriver"
 * )
 */
class VbCoreBaseLayout extends BootstrapLayout {
  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);

    // Add layout wrapper utility class
    $build['#theme_wrappers']['blb_container_wrapper']['#attributes']['class'][] = 'layout-wrapper';

    // Add anchor as layout wrapper id
    if (!empty($this->configuration['anchor'])) {
      $build['#theme_wrappers']['blb_container_wrapper']['#attributes']['id'] = Html::cleanCssIdentifier(strtolower($this->configuration['anchor']));
    }

    // Add section type class to layout wrapper
    if (!empty($this->configuration['type'])) {
      foreach (explode(' ', $this->configuration['type']) as $type) {
        $build['#theme_wrappers']['blb_container_wrapper']['#attributes']['class'][] = 'layout-wrapper--' . $type;
        $build['#attributes']['class'][] = 'layout--' . $type;
      }
    }

    // Remove '_none' classes from layout wrappers
    foreach ($build['#theme_wrappers']['blb_container_wrapper']['#attributes']['class'] as $i => $class) {
      if (empty($class) || $class == '_none') {
        unset($build['#theme_wrappers']['blb_container_wrapper']['#attributes']['class'][$i]);
      }
    }

    // Remove column classes from title region
    $build['title']['#attributes']['class'] = $this->configuration['regions_classes']['title'] ? [$this->configuration['regions_classes']['title']] : [];
    $build['title']['#attributes']['class'][] = 'layout-title'; 

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_configuration = parent::defaultConfiguration();
    return $default_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['ui']['nav_tabs']['client_settings'] = [
      '#type' => 'inline_template',
      '#template' => '<li><a data-target="{{ target|clean_class }}" class="{{active}}"><span role="img">{% include icon %}</span><div class="bs_tooltip" data-placement="bottom" role="tooltip">{{ title }}</div></a></li>',
      '#context' => [
        'title' => $this->t('Settings'),
        'target' => 'client-settings',
        'active' => '',
        'icon' => 'modules/contrib/bootstrap_styles/images/ui/settings.svg'
      ]
    ];
    $form['ui']['tab_content']['client_settings'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['bs_tab-pane', 'bs_tab-pane--client-settings']
      ]
    ];

    // Hide background type when there is only 1 or 0 options and remove collapsing wrapper
    if (!empty($form['ui']['tab_content']['appearance']['background']['background_type'])
      && count($form['ui']['tab_content']['appearance']['background']['background_type']['#options']) <= 1
    ) {
      $form['ui']['tab_content']['appearance']['background']['background_type']['#access'] = FALSE;
      $form['ui']['tab_content']['appearance']['background']['#type'] = 'container';
    }


    // Settings tab permission
    if (!\Drupal::currentUser()->hasPermission('access section settings')) {
      $form['ui']['nav_tabs']['settings']['#access'] = FALSE;
      $form['ui']['tab_content']['settings']['#access'] = FALSE;
    }

    // Type
    $form['ui']['tab_content']['settings']['type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Type'),
      '#default_value' => $this->configuration['type'] ?? '',
    ];

    // Anchor
    $form['ui']['tab_content']['client_settings']['anchor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Anchor'),
      '#default_value' => $this->configuration['anchor'] ?? '',
    ];

    // Container sizes
    if (isset($form['ui']['tab_content']['layout']['container_type'])) {
      $form['ui']['tab_content']['layout']['container_type']['#title'] = $this->t('Container size');
      $form['ui']['tab_content']['layout']['container_type']['#options'] = [
        'container container--small' => '<span class="input-icon container"></span>Small',
        'container container--medium' => '<span class="input-icon container"></span>Medium',
        'container container--large' => '<span class="input-icon container-fluid"></span>Large',
        'container-fluid' => '<span class="input-icon w-100"></span>Large',
      ];
      $form['ui']['tab_content']['layout']['container_type']['#default_value'] = $this->configuration['container'] ?: 'container container--large';
    }

    // Move locks to own tab
    if (isset($form['layout_builder_lock_wrapper'])) {
      $form['ui']['nav_tabs']['locks'] = [
        '#type' => 'inline_template',
        '#template' => '<li><a data-target="{{ target|clean_class }}" class="{{active}}"><span role="img">{% include icon %}</span><div class="bs_tooltip" data-placement="bottom" role="tooltip">{{ title }}</div></a></li>',
        '#context' => [
          'title' => $this->t('Locks'),
          'target' => 'locks',
          'active' => '',
          'icon' => \Drupal::service('extension.list.module')->getPath('bootstrap_styles') . '/images/ui/default.svg',
        ],
      ];
      $form['ui']['tab_content']['locks'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'bs_tab-pane',
            'bs_tab-pane--locks'
          ],
        ],
      ];

      $form['ui']['tab_content']['locks']['layout_builder_lock_wrapper'] = $form['layout_builder_lock_wrapper'];
      unset($form['layout_builder_lock_wrapper']);
    }

    // Define breakpoints.
    $breakpoints = ['desktop', 'tablet', 'mobile', 'extrasmall'];

    // Grid default values for new sections
    $default_values = [
      'blb_col_2' => [
        'desktop' => 'blb_col_6_6',
        'tablet' => 'blb_col_6_6',
        'mobile' => 'blb_col_12',
        'extrasmall' => 'blb_col_12',
      ],
      'blb_col_3' => [
        'desktop' => 'blb_col_4_4_4',
        'tablet' => 'blb_col_4_4_4',
        'mobile' => 'blb_col_12',
        'extrasmall' => 'blb_col_12',
      ],
      'blb_col_4' => [
        'desktop' => 'blb_col_3_3_3_3',
        'tablet' => 'blb_col_6_6',
        'mobile' => 'blb_col_12',
        'extrasmall' => 'blb_col_12',
      ],
      'blb_col_6' => [
        'desktop' => 'blb_col_2_2_2_2_2_2',
        'tablet' => 'blb_col_4_4_4_4_4_4',
        'mobile' => 'blb_col_6_6_6_6_6_6',
        'extrasmall' => 'blb_col_12',
      ],
    ];

    $layout = $form_state->getFormObject()->getCurrentLayout()->getPluginDefinition()->id();

    foreach ($breakpoints as $breakpoint) {
      if (isset($form['ui']['tab_content']['layout']['breakpoints'][$breakpoint])) {
        $form['ui']['tab_content']['layout']['breakpoints'][$breakpoint]['#default_value'] = $this->configuration['breakpoints'][$breakpoint] ?? $default_values[$layout][$breakpoint];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    // The tabs structure.
    $layout_tab = ['ui', 'tab_content', 'layout'];
    $style_tab = ['ui', 'tab_content', 'appearance'];
    $settings_tab = ['ui', 'tab_content', 'settings'];
    $client_settings_tab = ['ui', 'tab_content', 'client_settings'];

    // Save section type.
    $this->configuration['type'] = $form_state->getValue(array_merge($settings_tab, ['type']));

    // Save anchor.
    $this->configuration['anchor'] = $form_state->getValue(array_merge($client_settings_tab, ['anchor']));
  }
}
