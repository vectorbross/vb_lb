<?php

namespace Drupal\vb_lb\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Component\Utility\Html;


/**
 * Provides a 'Layout builder section anchors' block.
 *
 * @Block(
 *  id = "vb_lb_anchor_links",
 *  admin_label = @Translation("Layout builder section anchors"),
 *  category = @Translation("Vector BROSS"),
 * )
 */
class VbLayoutAnchorLinks extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $node = \Drupal::routeMatch()->getParameter('node');
    if (!$node instanceof \Drupal\node\NodeInterface) {
      return [];
    }

    if(!$node->hasField('layout_builder__layout')) {
      return [];
    }

    $items = [];

    foreach($node->layout_builder__layout->getValue() as $layout) {
      $settings = $layout['section']->getLayoutSettings();

      if(!empty($settings['anchor'])) {
        $items[] = [
          '#type' => 'inline_template',
          '#template' => '<a href="{{ link }}">{{ title }}</a>',
          '#context' => [
            'link' => '#' . Html::cleanCssIdentifier(strtolower($settings['anchor'])),
            'title' => $settings['anchor']
          ]
        ];
      }
    }

    if(empty($items)) {
      return [];
    }

    $build['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['sticky-nav'],
        'id' => 'scrollspy'
      ],
    ];

    $build['container']['item_list'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['item-list'],
      ],
      '#weight' => 2
    ];

    $build['container']['item_list']['nav'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $items,
      '#attributes' => [
        'class' => ['nav', 'nav-tabs'],
        'role' => 'tablist'
      ],
    ];


    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $build = $this->build();
    if (empty($build)) {
      return AccessResult::forbidden();
    }
    return parent::blockAccess($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      return Cache::mergeTags(parent::getCacheTags(), ['node:' . $node->id()]);
    }
    else {
      return parent::getCacheTags();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

}
