<?php

namespace Drupal\vb_lb\Plugin\Deriver;

use Drupal\bootstrap_layout_builder\Plugin\Deriver\BootstrapLayoutDeriver;

/**
 * Makes a bootstrap layout for each layout config entity.
 */
class VbCoreBaseLayoutDeriver extends BootstrapLayoutDeriver {
  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = parent::getDerivativeDefinitions($base_plugin_definition);

    foreach($this->derivatives as &$layout) {
      $regions = $layout->getRegions();
      $regions['title'] = [
        'label' => $this->t('title')
      ];
      $layout->setRegions($regions);
    }

    return $this->derivatives;
  }
}
