<?php

namespace Drupal\vb_lb\Controller;

use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\layout_builder\Context\LayoutBuilderContextTrait;
use Drupal\layout_builder\LayoutBuilderHighlightTrait;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\section_library\Entity\SectionLibraryTemplate;
use Drupal\Core\Render\Markup;
use Drupal\file\Entity\File;
use Drupal\section_library\Controller\ChooseSectionFromLibraryController;

/**
 * Defines a controller to choose a section from library.
 *
 * @internal
 *   Controller classes are internal.
 */
class VbPageTemplateLibraryController extends ChooseSectionFromLibraryController {

  /**
   * Gets a render array of section links.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The region the section is going in.
   *
   * @return array
   *   The section links render array.
   */
  protected function getSectionLinks(SectionStorageInterface $section_storage, $delta) {
    $sections = SectionLibraryTemplate::loadMultiple();
    $links = [];
    foreach ($sections as $section_id => $section) {

      // Skip sections, we only want to show templates here
      if(!$section->type->isEmpty() && $section->type->value == 'section') {
        continue;
      }

      $attributes = $this->getAjaxAttributes();
      $attributes['class'][] = 'js-layout-builder-section-library-link';
      // Default library image.
      $img_path = drupal_get_path('module', 'section_library') . '/images/default.png';
      if ($fid = $section->get('image')->target_id) {
        $file = File::load($fid);
        $img_path = $file->getFileUri();
      }

      $file_url = \Drupal::service('file_url_generator')->generateAbsoluteString($img_path);
      $icon_url = \Drupal::service('file_url_generator')->transformRelative($file_url);
      $layout = $section->layout_section->getValue()[0]['section']->toRenderArray([], TRUE);

      $link = [
        '#type' => 'inline_template',
        '#template' => '
          <a href="{{ url }}" class="js-layout-builder-section-library-link">
            <img src="{{ icon }}" class="section-library-link-img" />
            <span class="section-library-link-label">{{ label }}</span>
          </a>
          <div class="preview-toggle">View preview</div>
          <div class="preview">{{ preview }}</div>',
        '#context' => [
          'icon' => $icon_url,
          'label' => $section->label(),
          'preview' => \Drupal::service('renderer')->render($layout),
          'url' => Url::fromRoute('section_library.import_section_from_library',
            [
              'section_library_id' => $section_id,
              'section_storage_type' => $section_storage->getStorageType(),
              'section_storage' => $section_storage->getStorageId(),
              'delta' => $delta,
            ]
        ),
        ]
      ];

      $links[] = $link;
    }
    
    return [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $links,
      '#attributes' => [
        'class' => [
          'section-library-links',
        ],
      ],
    ];
  }
}
