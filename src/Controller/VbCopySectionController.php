<?php

namespace Drupal\vb_lb\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\Controller\LayoutRebuildTrait;
use Drupal\section_library\DeepCloningTrait;

/**
 * Class VbCopySectionController
 */
class VbCopySectionController implements ContainerInjectionInterface  {

  use AjaxHelperTrait;
  use LayoutRebuildTrait;
  use DeepCloningTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Private storage.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $privateTempStore;

  /**
   * The layout tempstore repository.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface
   */
  protected $layoutTempstoreRepository;

  /**
   * The UUID generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidGenerator;

  /**
   * Include the messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * ChooseSectionFromLibraryController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ExtensionList $extension_list_module
   *   The extension list module service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, PrivateTempStoreFactory $private_temp_store, LayoutTempstoreRepositoryInterface $layout_tempstore_repository, UuidInterface $uuid, MessengerInterface $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->privateTempStore = $private_temp_store->get('vb_lb');
    $this->layoutTempstoreRepository = $layout_tempstore_repository;
    $this->uuidGenerator = $uuid;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('tempstore.private'),
      $container->get('layout_builder.tempstore_repository'),
      $container->get('uuid'),
      $container->get('messenger'),
    );
  }

  /**
   * Copy a given section.
   */
  public function copy(SectionStorageInterface $section_storage, $delta) {
    $response = new AjaxResponse();

    if(isset($section_storage) && isset($delta)) {
      $section = $section_storage->getSections()[$delta];
      $this->privateTempStore->set('copied_section', $section);
      $this->messenger->addMessage(t('Section copied'));
      $response->addCommand(new RemoveCommand('.layout-builder__section [data-drupal-messages]'));
      $response->addCommand(new PrependCommand('.layout-builder__section:eq(' . $delta . ')', ['#type' => 'status_messages']));
    }
    return $response;
  }

  /**
   * Paste the section stored in tempstore.
   */
  public function paste(SectionStorageInterface $section_storage, $delta) {
    if($tempstore_section = $this->privateTempStore->get('copied_section')) {
      $tempstore_section_array = $tempstore_section->toArray();

      // Clone section.
      $cloned_section = new Section(
        $tempstore_section->getLayoutId(),
        $tempstore_section->getLayoutSettings(),
        $tempstore_section->getComponents(),
        $tempstore_section_array['third_party_settings']
      );

      // Replace section components with new instances.
      $deep_cloned_section = $this->cloneAndReplaceSectionComponents($cloned_section);

      // Create a new section.
      $section_storage->insertSection($delta, $deep_cloned_section);

      // Update layout builder tempstore so everything keeps working properly
      $this->layoutTempstoreRepository->set($section_storage);
    }

    if ($this->isAjax()) {
      return $this->rebuildAndClose($section_storage);
    }
    else {
      $url = $section_storage->getLayoutBuilderUrl();
      return new RedirectResponse($url->setAbsolute()->toString());
    }
  }
}
