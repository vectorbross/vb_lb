<?php

namespace Drupal\vb_lb\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;


/**
 * Class VbChooseBlockController
 */
class VbMoveBlockController extends ControllerBase  {

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {
    $parameters = \Drupal::routeMatch()->getParameters();

    // Do not allow blocks being moved to another section
    if($parameters->get('delta_from') != $parameters->get('delta_to')) {
      return AccessResult::allowedIfHasPermission('move block to other region');
    }

    return AccessResult::allowed();
  }
}
