<?php

namespace Drupal\vb_lb\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VbRenderLayoutBuilderController.
 *
 * Used to render the node and replace the layout builder rendered field
 * by the actual builder.
 */
class VbRenderLayoutBuilderController implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface;
   */
  protected $formBuilder;

  /**
   * VbRenderLayoutBuilderController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FormBuilderInterface $form_builder) {
    $this->entityTypeManager = $entity_type_manager;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('form_builder'),
    );
  }

  /**
   * Renders the node and replaces the Layout builder.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   *
   * @return array
   *   A render array.
   */
  public function build(SectionStorageInterface $section_storage) {
    $node = $section_storage->getContextValue('entity');
    $view_mode = $section_storage->getContextValue('view_mode');
    $view_builder = $this->entityTypeManager->getViewBuilder('node');
    $build = $view_builder->view($node, $view_mode);

    // Replace the rendered layout builder field by the actual builder.
    $build['_layout_builder'] = [
      '#type' => 'layout_builder',
      '#section_storage' => $section_storage,
    ];

    // Load the entity form we've previously unset from the route.
    $entity_form = $this->entityTypeManager
      ->getFormObject('node', 'layout_builder')
      ->setEntity($node);
    $lb_form = $this->formBuilder->getForm($entity_form, $section_storage);
    unset($lb_form['layout_builder__layout']);

    $build['_layout_builder']['entity_form'] = $lb_form;

    return $build;

  }

}

