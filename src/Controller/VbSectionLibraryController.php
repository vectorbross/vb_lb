<?php

namespace Drupal\vb_lb\Controller;

use Drupal\section_library\Controller\ChooseSectionFromLibraryController;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\section_library\Entity\SectionLibraryTemplate;
use Drupal\Core\Render\Markup;
use Drupal\file\Entity\File;
use Drupal\Core\Url;
use Drupal\section_library\Entity\SectionLibraryTemplateInterface;

/**
 * Defines a controller to choose a section from library.
 * 
 * We extend ChooseSectionFromLibraryController but overwrite the getSectionLinks function to make sure only sections are shown and no (page) templates
 */
class VbSectionLibraryController extends ChooseSectionFromLibraryController {

  /**
   * Gets a render array of section links.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The region the section is going in.
   *
   * @return array
   *   The section links render array.
   */
  protected function getSectionLinks(SectionStorageInterface $section_storage, $delta) {
    $sections = SectionLibraryTemplate::loadMultiple();
    $links = [];
    foreach ($sections as $section_id => $section) {
      /* @var $section SectionLibraryTemplateInterface */

      // Skip templates, we only want to show sections here
      if ($section->hasField('type') && !empty($section->get('type')->getValue())) {
        if ($section->get('type')->getString() == 'template') {
          continue;
        }
      }

      $attributes = $this->getAjaxAttributes();
      $attributes['class'][] = 'js-layout-builder-section-library-link';

      // Default library image.
      $img_path = \Drupal::service('extension.list.module')->getPath('vb_lb') . '/images/default.png';
      if ($fid = $section->get('image')->target_id) {
        $file = File::load($fid);
        $img_path = $file->getFileUri();
      }

      $file_url = \Drupal::service('file_url_generator')->generateAbsoluteString($img_path);
      $icon_url = \Drupal::service('file_url_generator')->transformRelative($file_url);
      $layout = $section->layout_section->getValue()[0]['section']->toRenderArray([], TRUE);

      $link = [
        '#type' => 'inline_template',
        '#template' => '
          <a href="{{ url }}" class="js-layout-builder-section-library-link">
            <img src="{{ icon }}" class="section-library-link-img" />
            <span class="section-library-link-label">{{ label }}</span>
          </a>
          <div class="preview-toggle">View preview</div>
          <div class="preview">{{ preview }}</div>',
        '#context' => [
          'icon' => $icon_url,
          'label' => $section->label(),
          'preview' => \Drupal::service('renderer')->render($layout),
          'url' => Url::fromRoute('section_library.import_section_from_library',
            [
              'section_library_id' => $section_id,
              'section_storage_type' => $section_storage->getStorageType(),
              'section_storage' => $section_storage->getStorageId(),
              'delta' => $delta,
            ]
        ),
        ]
      ];

      // $link = [
      //   'title' => Markup::create('<img src="' . $icon_url . '" class="section-library-link-img" /> ' . '<span class="section-library-link-label">' . $section->label() . '</span>'),
      //   'url' => Url::fromRoute('section_library.import_section_from_library',
      //     [
      //       'section_library_id' => $section_id,
      //       'section_storage_type' => $section_storage->getStorageType(),
      //       'section_storage' => $section_storage->getStorageId(),
      //       'delta' => $delta,
      //     ]
      //   ),
      //   'attributes' => $attributes,
      // ];

      $links[] = $link;
    }
    return [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $links,
      '#attributes' => [
        'class' => [
          'section-library-links',
        ],
      ],
    ];
  }
}
