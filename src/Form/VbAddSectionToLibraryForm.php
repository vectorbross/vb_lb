<?php

namespace Drupal\vb_lb\Form;

use Drupal\section_library\Form\AddSectionToLibraryForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;

/**
 * Provides a form for adding a section to the library.
 *
 * @internal
 *   Form classes are internal.
 */
class VbAddSectionToLibraryForm extends AddSectionToLibraryForm {

  /**
   * {@inheritdoc}
   */
  protected function successfulAjaxSubmit(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new CloseDialogCommand('.modal'));
    $response->addCommand(new InvokeCommand('body', 'removeClass', ['modal-open']));
    $response->addCommand(new InvokeCommand('body', 'css', ['padding-right', '0']));
    $response->addCommand(new InvokeCommand('body', 'css', ['padding-top', '0']));
    return $response;
  }
}