<?php

namespace Drupal\vb_lb\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Override the controller for layout_builder.move_block.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Replace layout builder entity form by our own controller
    if ($route = $collection->get('layout_builder.overrides.node.view')) {
      $defaults = $route->getDefaults();

      // Unset the default layout builder form.
      unset($defaults['_entity_form']);

      // Use our own controller which will render the node along with
      // the layout builder.
      $defaults['_controller'] = '\Drupal\vb_lb\Controller\VbRenderLayoutBuilderController::build';
      $route->setDefaults($defaults);
    }

    // Do not allow moving blocks to another section
    if ($route = $collection->get('layout_builder.move_block')) {
      $requirements = $route->getRequirements();
      $requirements['_custom_access'] = '\Drupal\vb_lb\Controller\VbMoveBlockController::access';
      $route->setRequirements($requirements);
    }

    // Add inline block filtering to parent class
    // Drupal\layout_builder\Controller\ChooseBlockController.
    if ($route = $collection->get('layout_builder.choose_block')) {
      $defaults = $route->getDefaults();
      $defaults['_controller'] = '\Drupal\vb_lb\Controller\VbChooseBlockController::build';
      $route->setDefaults($defaults);
    }

    // Overwrite section_library route to add sections from library to our own controller
    if ($route = $collection->get('section_library.choose_template_from_library')) {
      $defaults = $route->getDefaults();
      $defaults['_controller'] = '\Drupal\vb_lb\Controller\VbSectionLibraryController::build';
      $route->setDefaults($defaults);
    }

    // Overwrite section_library route to save section to library to our own form
    // so we can write our own AJAX callback
    if ($route = $collection->get('section_library.add_section_to_library')) {
      $defaults = $route->getDefaults();
      $defaults['_form'] = '\Drupal\vb_lb\Form\VbAddSectionToLibraryForm';
      $route->setDefaults($defaults);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    // Make sure Layout builders dynamic routes have been created
    // before we alter them.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -120];
    return $events;
  }

}
