<?php

namespace Drupal\vb_lb;

use Drupal\Core\Render\Element;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Drupal\node\NodeInterface;

/**
 * Implements preRender for layout builder.
 */
class VbLayoutBuilderRender implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRender'];
  }

  /**
   * Alters layout builder to only allow one text block in title regions
   */
  public static function preRender($element) {
    $lb = &$element['layout_builder'];
    $section_storage = $element['#section_storage'];
    $section_count = count($section_storage->getSections());

    // No sections found, attach the template library button and paste button
    if($section_count == 0 && isset($lb[1]['link'])) {
      $dialog_options = Json::encode([
        'width' => "800px",
        'height' => "auto",
        'target' => 'layout-builder-modal-choose-template',
        //'autoResize' => TRUE,
        //'modal' => TRUE,
      ]);

      $lb[0]['add_template_to_library']['#access'] = FALSE;
      $lb[1]['choose_template'] = [
        '#type' => 'link',
        '#title' => t('Page templates'),
        '#url' =>Url::fromRoute(
         'vb_lb.choose_template_from_library',
          [
            'section_storage_type' => $section_storage->getStorageType(),
            'section_storage' => $section_storage->getStorageId(),
            'delta' => 0,
          ],
          [
            'attributes' => [
              'class' => [
                'use-ajax',
                'layout-builder__link',
                'layout-builder__link--choose-template',
              ],
              'data-dialog-type' => 'dialog',
              'data-dialog-options' => $dialog_options
            ],
          ]
        )
      ];
      $lb[1]['paste'] = [
        '#type' => 'link',
        '#title' => t('Paste'),
        '#url' => Url::fromRoute('vb_lb.paste_section',
          [
            'section_storage_type' => $section_storage->getStorageType(),
            'section_storage' => $section_storage->getStorageId(),
            'delta' => 0,
          ],
        ),
        '#attributes' => [
          'class' => [
            'use-ajax',
            'layout-builder__link',
            'layout-builder__link--paste-section',
          ],
        ],
      ];
    }

    // Page has sections
    else {
      $delta = 0;

      // Loop over regions and sections to check for title regions
      foreach (Element::children($lb) as $section) {

        // Hide the page library button if user has no permission
        if (isset($lb[$section]['add_template_to_library'])) {
          if(!\Drupal::currentUser()->hasPermission('add page library templates')) {
            $lb[$section]['add_template_to_library']['#access'] = FALSE;
            continue;
          }
        }

        // Hide the library button if user has no permission
        if (isset($lb[$section]['choose_template_from_library'])) {
          if(!\Drupal::currentUser()->hasPermission('add library sections')) {
            $lb[$section]['choose_template_from_library']['#access'] = FALSE;
            continue;
          }
        }

        // Add the paste links
        if (isset($lb[$section]['link'])) {
          $lb[$section]['paste'] = [
            '#type' => 'link',
            '#title' => t('Paste'),
            '#url' => Url::fromRoute('vb_lb.paste_section',
              [
                'section_storage_type' => $section_storage->getStorageType(),
                'section_storage' => $section_storage->getStorageId(),
                'delta' => $delta,
              ],
            ),
            '#attributes' => [
              'class' => [
                'use-ajax',
                'layout-builder__link',
                'layout-builder__link--paste-section',
              ],
            ],
          ];
        }

        // Section found
        if (isset($lb[$section][0])) {

          $lb[$section]['copy'] = [
            '#type' => 'link',
            '#title' => t('Copy'),
            '#url' => Url::fromRoute('vb_lb.copy_section',
              [
                'section_storage_type' => $section_storage->getStorageType(),
                'section_storage' => $section_storage->getStorageId(),
                'delta' => $delta,
              ],
            ),
            '#attributes' => [
              'class' => [
                'use-ajax',
                'layout-builder__link',
                'layout-builder__link--copy-section',
              ],
            ],
          ];

          // Loop over regions
          foreach (Element::children($lb[$section][0]) as $region) {

            if($region == 'title') {

              // Hide title add button if user has no permission
              if(!\Drupal::currentUser()->hasPermission('add title blocks')) {
                $lb[$section][0][$region]['layout_builder_add_block']['#access'] = FALSE;
                continue;
              }

              // Unset each link that isn't a text block add link
              foreach($lb[$section][0][$region]['layout_builder_add_block']['list']['links']['#links'] as $index => &$link) {
                if($link['url']->isRouted()
                  && empty($link['url']->getRouteParameters()['plugin_id'])
                  || $link['url']->getRouteParameters()['plugin_id'] != 'inline_block:section_title'
                ) {
                  $lb[$section][0][$region]['layout_builder_add_block']['list']['links']['#links'][$index]['#access'] = FALSE;
                } else {

                  // If the region already has a block, we also unset the text block add link
                  $has_title = FALSE;

                  foreach ($lb[$section][0][$region] as $id => $child) {
                    if(isset($child['#base_plugin_id']) && $child['#base_plugin_id'] == 'inline_block') {
                      $has_title = TRUE;
                      continue;
                    }
                  }
                  
                  if($has_title) {
                    $lb[$section][0][$region]['layout_builder_add_block']['#access'] = FALSE;
                  } else {

                    // No title present so we overwrite the default layout_builder_add_block element
                    $link['attributes']['class'][] = 'layout-builder__link--add';
                    $lb[$section][0][$region]['layout_builder_add_block'] = [
                      '#type' => 'container',
                      '#attributes' => [
                        'class' => ['layout-builder__add-block', 'layout-builder__add-title']
                      ],
                      'link' => [
                        '#type' => 'link',
                        '#title' => t('Add title'),
                        '#url' => $link['url'],
                        '#attributes' => $link['attributes'],
                      ]
                    ];
                  }
                }
              }
            }

            // Hide block add buttons if user has no permission
            else {
              if(!\Drupal::currentUser()->hasPermission('add grid blocks')) {
                $lb[$section][0][$region]['layout_builder_add_block']['#access'] = FALSE;
                continue;
              }
            }
          }

          $delta++;
        }
      }
    }

    // Load library that overwrites default layout builder behavior to prevent moving blocks between regions
    if(!\Drupal::currentUser()->hasPermission('move block to other region')) {
      $element['#attached']['library'][] = 'vb_lb/prevent_move_block';
    }

    return $element;
  }
}
