<?php

namespace Drupal\vb_lb\EventSubscriber;

use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;


/**
 * Provides an event subscriber that alters Ajax Responses.
 */
class AjaxResponseSubscriber implements EventSubscriberInterface {

  /**
   * Close modal dialog if Layout Builder is re-rendered.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event.
   */
  public function onResponse(ResponseEvent $event) {
    $response = $event->getResponse();

    if ($response instanceof AjaxResponse) {
      $should_close_dialog = FALSE;

      $commands = &$response->getCommands();

      foreach ($commands as $command) {
        if (isset($command['selector']) && $command['selector'] === '#layout-builder') {
          $should_close_dialog = TRUE;
          break;
        }
      }

      if ($should_close_dialog) {
        $response->addCommand(new CloseDialogCommand('.modal'));
        $response->addCommand(new InvokeCommand('body', 'removeClass', ['modal-open']));
        $response->addCommand(new InvokeCommand('body', 'css', ['padding-right', '0']));
        $response->addCommand(new InvokeCommand('body', 'css', ['padding-top', '0']));
      }

    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::RESPONSE => [['onResponse']]];
  }

}
