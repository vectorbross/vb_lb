(function ($, Drupal, Sortable) {
	Drupal.behaviors.layoutBuilderBlockDrag = {
		attach: function attach(context) {

			if($(context).is('.layout-builder, .loading')) {
				$(context).removeClass('loading');
			}

			// This file is identical to the default layoutBuilderBlockDrag behavior but adds the parent layout id
			// to the Sortable group parameter. This is done to prevent moving blocks from one to another layout.

			var regionSelector = '.js-layout-builder-region';
			Array.prototype.forEach.call(context.querySelectorAll(regionSelector), function (region) {

				var parent = $(region).parent().data('layout-delta');
				Sortable.create(region, {
					draggable: '.js-layout-builder-block',
					ghostClass: 'ui-state-drop',
					group: 'builder-region-' + parent,
					onStart: function onStart(event) {
						// Add min height to target and sibling columns to impove UX
						var height = $(event.item).parent().height();
						$(event.target).css('min-height', height);
						$(event.target).siblings().css('min-height', height);

						// Highlight all columns where the block can be dropped;
						$(event.target).parent().addClass('highlight');
					},
					onEnd: function onEnd(event) {
						// Unset the min heights and highlight that we set earlier
						$('.layout-builder__region').css('min-height', '');
						$('.layout-builder__layout').removeClass('highlight');

						// Add loading class to show loader
						$('.layout-builder').addClass('loading');

						// Send request to Drupal
						return Drupal.layoutBuilderBlockUpdate(event.item, event.from, event.to);
					}
				});
			});
		}
	};
})(jQuery, Drupal, Sortable);
